# sjLogiTree
sjLogiTree is a logic tool leveraging binary trees for demonstrations, checks, and simplification of expressions. Aimed at exploring the fundamentals of logic, it's suited for educational and exploratory purposes in computational logic.
